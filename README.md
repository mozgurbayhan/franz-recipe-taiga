# Taiga.io for Franz
This is the non-official Franz recipe for Taiga.io

### How to use it:
Put the source folder under:

- Mac: ~/Library/Application Support/Franz/recipes/dev/
- Windows: %appdata%/Franz/recipes/dev/
- Linux: ~/.config/Franz/recipes/dev

then restart or refresh Franz. You will find the plugin in the menu: 
>Add new service -> Development

* [Documentation](https://github.com/meetfranz/plugins/blob/master/docs/integration.md#user-content-installation) 

### How to create your own Franz recipes:
* [Read the documentation](https://github.com/meetfranz/plugins)
